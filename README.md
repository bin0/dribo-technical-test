### Fullstack JavaScript Exercise

- Create a service with and endpoint that accepts `$ curl localhost -d '{ "username", "password" }'`  :white_check_mark:
- If `$ curl localhost` is requested then return a login form with two fields ['username', 'password'] :white_check_mark:
- Field validations must be done:
  - username must match this regexp /\w+#\d+/ :white_check_mark:
  - password must match this regexp /\w{8,}/ :white_check_mark:
- Field validations must occur both client and server side :white_check_mark:
  - any clues to avoid writing same code twice? :x:
    - Didn't find a simple way to share validation code :(. 
- Login form must have following behaviors: 
  - password input should toggle `type="password"` when holding click :white_check_mark:
  - submit button should be disabled when validation not meet :white_check_mark:
  - submit button should be disabled during request and display an spinner too :white_check_mark:

### Important files and folders:
* .eslintrc.js: file to define the eslint rules to apply
* README.md: file to describe the proyeto purpose and the important project information
* index.js: main app file. It initialize the express app and run the needed configuration(middlewares, routest,..)
* bin: contains js file to be executed from package.json
* config: contains config files
* middlewares: contains general middlewares for the project(express)
* api: contains the proyect functionality:

  * index.js: add routes in proyect
  * routes-boot: wrapper to initialize and configure all available routes
  * services: contains the services availables for being used in the controllers:

    * index.js: exports all available services
    * service-structure: every service must have its own folder. Inside the folder it should have an index which exports a service's instance. Each service is resposible to require its dependencies and build an instance

  * controllers: contains all controllers in the app. Each controller will have method that will mach wit available http routes:

    * index:js: it is responsible of export all routes config. It will require the available services and it can pass them to each controller to be used as dependencies.
    * controller-structure: each controller should be in its own folfer. Each folder should have a file (o several if the scope grows up an it is necesary to split the content) for the code and another for the tests. It also shold have and index.js, this file will be resonsible for creating the controller instance with the services pased as parameters and to export an array of available routes and and hanlder methods. Each element in this array should have:

      * method: http method
      * path: url path
      * handler: controller's method to handle the request
      * bindTo: controller instance
      * middlewares: array , list of method to use as middlewares
      * skipDefaultMiddlewares: boolean to skip or not de default middlewares(added in routes-boot.js)


# Instalar
 * npm install

# Arrancar
 * npm start

## Localhost
 * Serve on http://localhost:3000
