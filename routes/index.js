var express = require('express');
var router = express.Router();
var { validPassword, validUsername } = require('../utils/validations');

var title = 'DRIBO';
/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title });
});

/* POST Receive form */
router.post('/', function(req, res) {
  const { username, password } = req.body;

  if (!validUsername(username) && !validPassword(password)) {
    throw new Error('Usuario o contraseña incorrectos');
  }

  res.render('index', {
    title,
    message: `Gracias ${username}`,
    fullfilled: true,
  });
});

module.exports = router;
