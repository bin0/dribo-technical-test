function validUsername(username) {
  return /\w+#\d+/.test(username);
}
function validPassword(password) {
  return /\w+#\d+/.test(password);
}

module.exports = {
  validUsername,
  validPassword,
};
