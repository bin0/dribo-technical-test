document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('send-button').disabled = true;
});

var inputs = {
  username: null,
  password: null,
};

function validUsername(username) {
  return /\w+#\d+/.test(username);
}

function validPassword(password) {
  return /\w{8,}/.test(password);
}

function handleMouseDown(e) {
  e.type = 'text';
}

function handleMouseUp(e) {
  e.type = 'password';
}

function handleChangeInput({ value, id }, fn) {
  inputs[id] = value;

  if (!fn(inputs[id])) {
    document.getElementById('send-button').disabled = true;
  } else {
    if (validUsername(inputs.username) && validPassword(inputs.password)) {
      document.getElementById('send-button').disabled = false;
    }
  }
}

function validateForm() {
  const sendButton = document.getElementById('send-button');

  sendButton.type = 'image';
  sendButton.src = 'images/spinner.gif';
  return true;
}
